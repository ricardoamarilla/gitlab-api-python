#!/usr/bin/env python

# Description: Showcases how to use object-oriented code and helper functions to serialize JSON objects, logging, etc.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
import json

# Print an error message with prefix, and exit immediately with an error code.
def error(text):
    logger("ERROR", text)
    sys.exit(1)

# Log a line with a given prefix (e.g. INFO)
def logger(prefix, text):
    print("{prefix}: {text}".format(prefix=prefix, text=text))

# Return a pretty-printed JSON string with indent of 4 spaces
def render_json_output(data):
    return json.dumps(data, indent=4, sort_keys=True)

# Class definition
class GitLabUseCase(object):
    # Initializer to set all required parameters
    def __init__(self, verbose, gl_server, gl_token, gl_project_id):
        self.verbose = verbose
        self.gl_server = gl_server
        self.gl_token = gl_token
        self.gl_project_id = gl_project_id

    # Debug logger, controlled via verbose parameter
    def log_debug(self, text):
        if self.verbose:
            print("DEBUG: {d}".format(d=text))

    # Connect to the GitLab server and store the connection handle
    def connect(self):
        self.log_debug("Connecting to GitLab API at {s}".format(s=self.gl_server))
        # Supports personal/project/group access token
        # https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens
        self.gl = gitlab.Gitlab(self.gl_server, private_token=self.gl_token)

    # Use the stored connection handle to fetch a project object by id,
    # and print its attribute with JSON pretty-print.
    def print_project_attributes(self):
        project = self.gl.projects.get(self.gl_project_id)
        print(render_json_output(project.attributes))


## main
if __name__ == '__main__':
    # Fetch configuration from environment variables.
    # The second parameter specifies the default value when not provided.
    gl_verbose = os.environ.get('GL_VERBOSE', False)
    gl_server = os.environ.get('GL_SERVER', 'https://gitlab.com')

    gl_token = os.environ.get('GL_TOKEN')

    if not gl_token:
        error("Please specifiy the GL_TOKEN env variable")

    gl_project_id = os.environ.get('GL_PROJECT_ID', 42491852) # https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-python

    # Instantiate new object and run methods
    gl_use_case = GitLabUseCase(gl_verbose, gl_server, gl_token, gl_project_id)
    gl_use_case.connect()
    gl_use_case.print_project_attributes()
