#!/usr/bin/env python

# Description: Fetch projects (all, group, project), and print the job artifacts summary.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import datetime
import gitlab
import os
import sys
import time
import json 

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires developer permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
# https://gitlab.com/gitlab-de/playground/artifact-gen-group
GROUP_ID = os.environ.get('GL_GROUP_ID') #optional

def render_size_mb(v):
    return "%.4f" % (v / 1024 / 1024)

def render_age_time(v):
    return str(datetime.timedelta(seconds = v))

def get_threshold_size():
    return 10 * 1024 * 1024

def get_threshold_age():
    return 90 * 24 * 60 * 60

def calculate_age(created_at_datetime):
    created_at_ts = datetime.datetime.strptime(created_at_datetime, '%Y-%m-%dT%H:%M:%S.%fZ')
    now = datetime.datetime.now()
    return (now - created_at_ts).total_seconds()
    


#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN, pagination="keyset", order_by="id", per_page=100)

    print("# Starting...", end="", flush=True)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []
    groups = []

    # Direct project ID
    if PROJECT_ID:
        print("DEBUG: PROJECT_ID")
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        print("DEBUG: GROUP_ID")
        group = gl.groups.get(GROUP_ID)

        groups.append(group.subgroups.list())

        for project in group.projects.list(include_subgroups=True, get_all=True):
            print(".", end="", flush=True) # Get some feedback that it is still looping
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id , lazy=True)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        print("DEBUG: all projects")
        projects = gl.projects.list(get_all=True)

    print(f"\n# Found {len(projects)} projects. Analysing...", end="", flush=True) # new line

    ci_job_artifacts = []
    jobs_marked_delete_artifacts = []

    for project in projects:
        project_obj = gl.projects.get(project.id, statistics=True)

        print("### Project {n} statistics\n {s}\n".format(n=project_obj.name_with_namespace, s=json.dumps(project_obj.statistics, indent=4)))

        # Important: Large result set requires iterator and pagination
        # https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination 
        jobs = project.jobs.list(pagination="keyset", order_by="id", per_page=100, iterator=True)

        print("### Job artifacts\n")
        for job in jobs:
            artifacts = job.attributes['artifacts']
            print("DEBUG: ID {i}: {a}".format(i=job.id, a=json.dumps(artifacts, indent=4)))
            if not artifacts:
                continue 

            # Advanced filtering: Age and Size
            # Example: 90 days, 10 MB threshold (TODO: Make this configurable)
            threshold_age = 90 * 24 * 60 * 60
            threshold_size = 10 * 1024 * 1024

            # job age, need to parse API format: 2023-08-08T22:41:08.270Z
            #created_at = datetime.datetime.strptime(job.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
            #now = datetime.datetime.now()
            #age = (now - created_at).total_seconds()            
            # Shorter: Use a function 
            age = calculate_age(job.created_at)

            # 'artifacts': [{'file_type': 'trace', 'size': 2697, 'filename': 'job.log', 'file_format': None}] 
            for a in artifacts:
                data = {
                    "project_id": project_obj.id,
                    "project_web_url": project_obj.name,
                    "project_path_with_namespace": project_obj.path_with_namespace,
                    "job_id": job.id,
                    "age": age,
                    "artifact_filename": a['filename'],
                    "artifact_file_type": a['file_type'],
                    "artifact_size": a['size']
                }

                ci_job_artifacts.append(data)

                # Advanced filtering: match job artifacts age and size against thresholds
                if (float(age) > float(threshold_age)) or (float(a['size']) > float(threshold_size)):
                    # mark job for deletion (cannot delete inside the loop)
                    jobs_marked_delete_artifacts.append(job)                

            print(".", end="", flush=True) # Get some feedback that it is still looping

        # Advanced example: pipelines
        print("### Pipelines\n\n")
        for pipeline in project.pipelines.list(iterator=True):
            pipeline_obj = project.pipelines.get(pipeline.id)
            print("DEBUG: {p}".format(p=json.dumps(pipeline_obj.attributes, indent=4)))

            #created_at = datetime.datetime.strptime(pipeline.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
            #now = datetime.datetime.now()
            #age = (now - created_at).total_seconds()
            age = calculate_age(pipeline.created_at)

            threshold_age = 90 * 24 * 60 * 60

            if (float(age) > float(threshold_age)):
                # Delete pipeline
                print("Deleting pipeline", pipeline.id)
                #pipeline_obj.delete()

            print(".", end="", flush=True) # Get some feedback that it is still

        # Advanced example: Container registries https://python-gitlab.readthedocs.io/en/stable/gl_objects/repositories.html 
        print("### Container registry\n\n")
        repositories = project.repositories.list()
        if len(repositories) > 0:
            repository = repositories.pop()
            tags = repository.tags.list(iterator=False)

            for tag in tags:
                print(tag.attributes)
                #print("Tag {n}, created at {d}, size {s}".format(n=tag.name, d=tag.attributes['created_at'], s=tag.size))

            # Advanced example: Container

            # Cleanup: Keep only the latest tag 
            #repository.tags.delete_in_bulk(keep_n=1)
            # Cleanup: Delete all tags older than 1 month
            #repository.tags.delete_in_bulk(older_than="1m")
            # Cleanup: Delete all tags matching the regex `v.*`, and keep the latest 2 tags
            #repository.tags.delete_in_bulk(name_regex_delete="v.+", keep_n=2)

        # Advanced example: Package registry. Note: ordber_by is required by the method.
        packages = project.packages.list(order_by="created_at")
        print("### Package registry\n\n")
        for package in packages:
            package_size = 0.0

            package_files = package.package_files.list()
            for package_file in package_files:
                print("Package name: {p} File name: {f} Size {s}".format(
                    p=package.name, f=package_file.file_name, s=render_size_mb(package_file.size)))
                
                package_size =+ package_file.size

            print("Package size: {s}\n\n".format(s=render_size_mb(package_size)))

            threshold_size = 10 * 1024 * 1024

            if (package_size > float(threshold_size)):
                print("Package size {s} > threshold {t}, deleting package.".format(
                    s=render_size_mb(package_size), t=render_size_mb(threshold_size)))
                #package.delete()

            threshold_age = 90 * 24 * 60 * 60
            package_age = created_at = calculate_age(package.created_at)

            if (float(package_age > float(threshold_age))):
                print("Package age {a} > threshold {t}, deleting package.".format(
                    a=render_age_time(package_age), t=render_age_time(threshold_age)))
                #package.delete()



    # Advanced example: Group storage
    # Container registry - TODO  
    for group in groups:
        print(group.id) 

    print("\nDone collecting data.")

    # Advanced filtering: Delete all job artifacts marked to being deleted. 
    print("### Delete job artifacts\n\n")
    for job in jobs_marked_delete_artifacts:
        # delete the artifact
        #print("DEBUG", job)
        print("Job name {n}".format(n=job.name))
        #job.delete_artifacts()    

        # Sleep for 1 second
        time.sleep(1)


    if len(ci_job_artifacts) > 0:
        print("### Summary\n\n")
        print("|Project|Job|Artifact name|Artifact type|Artifact size|\n|-|-|-|-|-|") # Start markdown friendly table
        for artifact in ci_job_artifacts:
            print('| [{project_name}]({project_web_url}) | {job_name} | {artifact_name} | {artifact_type} | {artifact_size} |'.format(project_name=artifact['project_path_with_namespace'], project_web_url=artifact['project_web_url'], job_name=artifact['job_id'], artifact_name=artifact['artifact_filename'], artifact_type=artifact['artifact_file_type'], artifact_size=render_size_mb(artifact['artifact_size'])))
    else:
        print("No artifacts found.")
