#!/usr/bin/env python

# Description: Recursively fetch the group information (path, URL), and all parent group information from a given group ID. Prints all group members too. 
# Requirements: python-gitlab Python libraries. GitLab API read access, developer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

def get_parent_group_and_print_group(gl, gid):
    if not gid:
        return
    
    group = gl.groups.get(gid)

    g_id = group.id
    url = group.web_url
    path = group.path 
    parent = group.parent_id
    members = group.members_all.list(get_all=True)

    print("Group ID {g_id}, Name: {n} {u} Parent ID: {p} Members (usernames, incl ancestors): {m}\n\n".format(
        g_id=g_id, n=path, u=url,p=parent, m=",".join([ x.username for x in members ])))

    return get_parent_group_and_print_group(gl, parent)

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/remote-development
GROUP_ID = os.environ.get('GL_GROUP_ID', 67293789)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

# TOKEN requires maintainer+ permissions
gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

get_parent_group_and_print_group(gl, GROUP_ID)

print("Success. Last line is the top level group namespace")
